extends Node2D

var timeout

func _ready():
	timeout = false

func _process(delta):
	if $Sprite/RayCast2D.is_colliding():
		fire()

func fire():
	if not timeout:
		$Sprite/RayCast2D.add_child(load(Global.Lighting).instance())
		$Sprite/Timer.start()
		timeout = true

func _on_Timer_timeout():
	timeout = false
